# usage
sudo bash test_script.sh --install

	--install          to install the lamp stack
	--start            to start web server and DB server
	--stop             to stop web server and DB server
	--status           to show the status of web service and DB service
	--remove           to remove apache2 and mysql
	--configure        to configure database
	--backup		   to take database backup